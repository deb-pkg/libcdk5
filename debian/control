Source: libcdk5
Section: libs
Priority: optional
Maintainer: Jose G. López <josgalo@gmail.com>
Build-Depends: debhelper (>= 8), libncurses5-dev, autotools-dev
Standards-Version: 3.9.6
Homepage: http://invisible-island.net/cdk
Vcs-Git: git://gitorious.org/deb-pkg/libcdk5.git
Vcs-Browser: https://gitorious.org/deb-pkg/libcdk5

Package: libcdk5
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C-based curses widget library
 CDK stands for "Curses Development Kit". CDK sits on top of the curses
 library and provides 22 ready to use widgets for rapid application
 development of text-based interfaces. CDK delivers many of the common
 widget types required for a robust interface. Widgets can be combined
 to create complex widgets if needed.

Package: libcdk5-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends}, libcdk5 (= ${binary:Version}), libncurses5-dev
Replaces: libcdk-examples, libcdk-dev
Conflicts: libcdk-examples, libcdk-dev
Description: C-based curses widget library (development files)
 CDK stands for "Curses Development Kit". CDK sits on top of the curses
 library and provides 22 ready to use widgets for rapid application
 development of text-based interfaces. CDK delivers many of the common
 widget types required for a robust interface. Widgets can be combined
 to create complex widgets if needed.
 .
 This package contains the header files and development libraries for
 the CDK library.
